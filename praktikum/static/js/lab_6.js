// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value= null;
   } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if(x == "log"){
        print.value = Math.log10(print.value);
  } else if(x=="sin"){
        print.value = Math.sin(print.value);
  } else if(x=="tan"){
        print.value = Math.tan(print.value);
  }
  else {
    print.value += x;
  }

};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//chat box
var head=document.getElementsByClassName('chat-head');
var body=document.getElementsByClassName('chat-body');
$(head[0]).click(function(){
	$(body[0]).slideToggle(300);
})

var sender=true;
$("textarea").bind("keyup", function(e){
  console.log(e.keyCode);
  if (e.keyCode==13){
    var string=$("textarea").val();
    $("textarea").val('');
    if (sender){
      $(".msg-insert").append("<div class=\"msg-send\">" + string + "</div>")
      sender=false;
    }
    else{
      $(".msg-insert").append("<div class=\"msg-receive\">" + string + "</div>")
      sender=true;
    }

  }
})
//theme
var storage=[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#FAFAFA"},
{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#FAFAFA"},
{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#FAFAFA"},
{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#FAFAFA"},
{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#FAFAFA"},
{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#FAFAFA"},
{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]

var index;
function changeTheme(newTheme){
  if (newTheme == null){
    if(!('theme' in localStorage)){
      index=storage[3];
    }
    else{
      index=JSON.parse(localStorage.getItem('theme'));
    }
}
else{
  index=storage[newTheme];
}
$('body').css({"backgroundColor": index.bcgColor});
$('.text-center').css({"color": index.fontColor});
$('.my-select').select2().val(index.id).change();
}

$(document).ready(function() {
    $('.my-select').select2({'data' : storage});
    changeTheme();
    $('.apply-button').on('click', function(){
    theme = $('.my-select').val();
    changeTheme(theme);
    localStorage.setItem('theme',JSON.stringify(storage[theme]));
    })
});

