 // FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '307312493098986',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)


  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      render(true);
    }else {
      facebookLogin();
    }
  })


  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('title').html(user.name + ' | Facebook');
      document.body.style.backgroundColor='white';
      $("body").css({background-image:none});
      $('.navigation-list').append('<li><a onclick="facebookLogout()">Log out</a></li>')
      $('.navbar-brand').html('Facebook');
      $('.login-heading').html('');
      $('div#lab8').html(
        '<div class="profile">' +
          '<div class="data">' +
              '<div id="introduction">'+
                  '<img class="picture" src="'+ user.picture.data.url + '"/>'+
                  '<h3 style ="padding-left:30px;">' + user.name + '</h3>'+
              '</div>'+
              '<table class="table">'+
              '<tbody>'+
                '<tr>'+
                  '<td><h3> Birthday </h3></td>' +
                  '<td><h3>' + user.birthday + '</h3></td>' +
                '</tr>'+
                '<tr>'+
                  '<td><h3> Email </h3></td>' +
                  '<td><h3>' + user.email + '</h3></td>' +
                '</tr>'+
                '<tr>'+
                '<td><h3> Gender </h3></td>' +
                '<td><h3>' + user.gender + '</h3></td>' +
              '</tr>'+
              '</tbody>'+
            '</table>'+
          '</div>' +
        '</div>' +
        '<input type="text" class="form-control form-control-lg post" id="postInput" " placeholder="Whats on your mind?">'+
        '<button class="btn btn-primary postStatus" onclick="postStatus()">Post to Facebook</button>'+
        '<div class="feeds">'+
          '<h1>News Feed</h1>'+
        '</div>'+
        '<div class="d-flex flex-xl-column">');

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
                '<button id="btn-delete" class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<button id="btn-delete" class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h2>' + value.story + '</h2>' +
                '<button id="btn-delete" class="btn btn-danger delete" onclick="deleteStatus(\''+ value.id + '\')">Delete</button>'+
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  FB.login(function(response){
    setTimeout(function(){ location.reload(true); },0);
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})

};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.logout(function(response){
    document.location.reload(true);
  });
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,email,gender,about', 'GET', function(response){
        console.log(response);
        fun(response);
        });
      }
  });
};
const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
    FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me/feed', 'GET', function(response){
        console.log(response);
        fun(response);
        });
      }
  });
};

const postFeed = () => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  var message = $('#postInput').val();
  FB.api('/me/feed', 'POST', {message:message});
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
  document.location.reload();
};



/**window.fbAsyncInit = () => {
    FB.init({
      appId      : '307312493098986',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    FB.getLoginStatus((response) => {
      if(response.status === 'connected'){
        render(true);
      }else{
        render(false);  
      }
    })  
  };*/


 /** $('title').html(user.name + ' | Facebook');
        $('.navbar-brand').html('Facebook');
        $('.login-heading').html('');
        $('div#lab8').html(
          '<div class="profile">' +
            '<div class="data">' +
                '<div id="introduction">'+
                    '<img class="picture" src="'+ user.picture.data.url + '"/>'+
                    '<h3 style ="padding-left:30px;">' + user.name + '</h3>'+
                '</div>'+
                '<table class="table">'+
                '<tbody>'+
                  '<tr>'+
                    '<td><h3> Birthday </h3></td>' +
                    '<td><h3>' + user.birthday + '</h3></td>' +
                  '</tr>'+
                  '<tr>'+
                    '<td><h3> Email </h3></td>' +
                    '<td><h3>' + user.email + '</h3></td>' +
                  '</tr>'+
                  '<tr>'+
                  '<td><h3> Gender </h3></td>' +
                  '<td><h3>' + user.gender + '</h3></td>' +
                '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>' +
          '</div>' +
          '<input type="text" class="form-control form-control-lg post" id="postInput" " placeholder="Whats on your mind?">'+
          '<button class="btn btn-primary postStatus" onclick="postStatus()">Post to Facebook</button>'+
          '<div class="feeds">'+
            '<h1>News Feed</h1>'+
          '</div>'+
          '<div class="d-flex flex-xl-column">'*/