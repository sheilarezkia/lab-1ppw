from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .api_enterkomputer import get_drones, get_soundcards, get_optical
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token
import environ

root = environ.Path(__file__) - 3 
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')


# Create your tests here.
class UnitTest(TestCase):
	def setUp(self):
		self.username = env("SSO_USERNAME")
		self.password = env("SSO_PASSWORD")

	def test_lab_9_url_is_exist(self):
		response = Client().get('/lab-9/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_using_index_func(self):
		found = resolve('/lab-9/')
		self.assertEqual(found.func, index)

	#############################################LOG IN & LOG OUT TEST##################################################

	def test_login_failed(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': "siapa", 'password': "saya"})
		html_response = self.client.get('/lab-9/').content.decode('utf-8')

	def test_page_when_user_is_logged_in_and_not_logged_in(self):
		#assure render login template when user is not logged in
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed('lab_9/session/login.html')

		#test if user is redirected to profile when they are logged in
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		#check if user is also redirected to profile when they try to access home page of lab 9
		response = self.client.get('/lab-9/')
		self.assertEqual(response.status_code, 302)
		self.assertTemplateUsed('lab_9/session/profile.html')

	def test_access_to_profile_url(self):
		#redirect to login page when user is not logged in and try to access profile page
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 302)

		#logged in, render profile template when user tries to access login page
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/profile/')
		self.assertEqual(response.status_code, 200)

	###################################################DRONES TEST#######################################################	

	def test_add_delete_and_reset_favorite_drones(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		#adds drones and make sure user is redirected after adding a drone
		response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil tambah drone favorite", html_response)

		#deletes drones and test if user is redirected after deleting a drone
		response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#resets drones after being added as favourites
		response = self.client.post('/lab-9/clear_session_drones/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite drones", html_response)

	def test_logout_success(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)
		response = self.client.post('/lab-9/custom_auth/logout/')
		html_response = self.client.get('/lab-9/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Anda berhasil logout. Semua session Anda sudah dihapus", html_response)

	##################################################CSUI_HELPER TEST###################################################

	def test_invalid_sso(self):
		user = "sheila"
		passw = "rezkia"
		with self.assertRaises(Exception) as context:
			get_access_token(user, passw)
		self.assertIn("sheila", str(context.exception))

	#######################################################COOKIE TEST####################################################

	def test_cookie(self):
		#not logged in
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 302) #redirect to log in page if user has not logged in with sso

		#login with HTTP GET method
		response = self.client.get('/lab-9/cookie/auth_login/')
		self.assertEqual(response.status_code, 302)

		#login failed because password and username combination is invalid
		response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'u', 'password': 'p'})
		html_response = self.client.get('/lab-9/cookie/login/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Username atau Password Salah", html_response)

		#try to set manual cookies
		self.client.cookies.load({"user_login": "u", "user_password": "p"})
		response = self.client.get('/lab-9/cookie/profile/')
		html_response = response.content.decode('utf-8')
		self.assertIn("Kamu tidak punya akses :P ", html_response)

		#login successed, make sure user is redirected to cookie profile page if they try to access login page
		#make sure user is not redirected when accessing profile cookie page
		self.client = Client()
		response = self.client.post('/lab-9/cookie/auth_login/', {'username': 'lab9plisselesai', 'password': 'sheila'})
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/cookie/login/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab-9/cookie/profile/')
		self.assertEqual(response.status_code, 200)

		#logout, redirect user when they log out
		response = self.client.post('/lab-9/cookie/clear/')
		html_response = self.client.get('/lab-9/cookie/profile/').content.decode('utf-8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Anda berhasil logout. Cookies direset", html_response)


	def test_add_delete_and_reset_favorite_soundcards(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		#add soundcard
		response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcards().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_soundcards/'+get_soundcards().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil tambah soundcards favorite", html_response)

		#delete soundcard
		response = self.client.post('/lab-9/del_session_soundcards/'+get_soundcards().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#reset soundcards
		response = self.client.post('/lab-9/clear_session_soundcards/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite soundcards", html_response)

	def test_add_delete_and_reset_favorite_optical(self):
		response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
		self.assertEqual(response.status_code, 302)

		#add optical
		response = self.client.post('/lab-9/add_session_optical/'+get_optical().json()[0]["id"]+'/')
		response = self.client.post('/lab-9/add_session_optical/'+get_optical().json()[1]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil tambah optical favorite", html_response)

		#delete optical
		response = self.client.post('/lab-9/del_session_optical/'+get_optical().json()[0]["id"]+'/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil hapus dari favorite", html_response)

		#reset opticals
		response = self.client.post('/lab-9/clear_session_optical/')
		html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
		self.assertEqual(response.status_code, 302)
		self.assertIn("Berhasil reset favorite optical", html_response)