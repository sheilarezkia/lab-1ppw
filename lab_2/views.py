from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = 'I am still learning a thing about this and i have never made a website before this one. if you happen to see any flaws or errors, i am sorry i probably have no idea how to fix it, either way i hope you are happy'

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
