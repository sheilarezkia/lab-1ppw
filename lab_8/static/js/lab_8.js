 // FB initiation function
window.fbAsyncInit = () => {
  FB.init({
    appId      : '307312493098986',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
  // dan jalankanlah fungsi render di bawah, dengan parameter true jika
  // status login terkoneksi (connected)


  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      render(true);
    }else {
      facebookLogin();
    }
  })


  // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
  // otomatis akan ditampilkan view sudah login
};

// Call init facebook. default dari facebook
(function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "https://connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));

// Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
// merender atau membuat tampilan html untuk yang sudah login atau belum
// Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
// Class-Class Bootstrap atau CSS yang anda implementasi sendiri
const render = loginFlag => {
  if (loginFlag) {
    // Jika yang akan dirender adalah tampilan sudah login

    // Memanggil method getUserData (lihat ke bawah) yang Anda implementasi dengan fungsi callback
    // yang menerima object user sebagai parameter.
    // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
    getUserData(user => {
      // Render tampilan profil, form input post, tombol post status, dan tombol logout
      $('title').html(user.name + ' | Facebook');
      document.body.style.backgroundColor='white';
      document.body.style.overflow='auto';
      document.body.style.backgroundImage='none';
      $('.navigation-list').append('<li><a onclick="facebookLogout()">Log out</a></li>')
      $('.navbar-brand').html('Facebook');
      $('.login-heading').html('');
      $('#lab8').html(
        '<div class="profile">' +
            '<div id="introduction">'+
                '<div><img class="picture" src="'+ user.picture.data.url + '"/></div>'+
                '<div><h2>' + user.name + '</h2></div>'+
            '</div>'+
            '<div class="sectionHeader">'+
              '<h3> Info </h3>' +
            '</div>'+
            '<div id = "info-content">'+
              '<h4> <span>Birthday</span>' + user.birthday+ '</h4>' +
              '<h4> <span>Email</span>' + user.email +'</h4>' +
              '<h4> <span>Gender</span>' + user.gender +'</h4>' +
            '</div>'+
            '<div id="addStatus">'+
              '<input type="text" class="form-control form-control-lg post" id="postInput" " placeholder="Whats on your mind?">'+
              '<button class="btn btn-primary postStatus" onclick="postStatus()">Post to Facebook</button>'+
            '</div>'+
        '</div>' +
        '<div class="feeds">'+
          '<h2>News Feed</h2>'+
          '<hr>'+
        '</div>');

      // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
      // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
      // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
      // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render feed, kustomisasi sesuai kebutuhan.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<p>' + value.message + '</p>' +
                '<h4>' + value.story + '</h4>' +
              '</div>'

            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed">' +
                '<p>' + value.message + '</p>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed">' +
                '<h4>' + value.story + '</h4>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // Tampilan ketika belum login
    $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
  }
};

const facebookLogin = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
  // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
  // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
  FB.login(function(response){
    setTimeout(function(){ location.reload(true); },0);
  }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})

};

const facebookLogout = () => {
  // TODO: Implement Method Ini
  // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
  // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
  FB.logout(function(response){
    document.location.reload(true);
  });
};

// TODO: Lengkapi Method Ini
// Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
// lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di 
// method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan 
// meneruskan response yang didapat ke fungsi callback tersebut
// Apakah yang dimaksud dengan fungsi callback?
const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,name,cover,picture,email,gender,about', 'GET', function(response){
        console.log(response);
        fun(response);
        });
      }
  });
};
const getUserFeed = (fun) => {
  // TODO: Implement Method Ini
  // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
  // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
  // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
  // tersebut
    FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me/feed', 'GET', function(response){
        console.log(response);
        fun(response);
        });
      }
  });
};

const postFeed = () => {
  // Todo: Implement method ini,
  // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
  // Melalui API Facebook dengan message yang diterima dari parameter.
  var message = $('#postInput').val();
  FB.api('/me/feed', 'POST', {message:message});
};

const postStatus = () => {
  const message = $('#postInput').val();
  postFeed(message);
  document.location.reload();
};
